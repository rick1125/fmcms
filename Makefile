.PHONY: update
folder=../.composer

all: $(folder)
	runuser -c "/usr/local/bin/composer install --prefer-dist --no-dev" www-data
$(folder):
	mkdir $@ && chown www-data.www-data $@
install:
	rm -fr app/cache/prod
	runuser -c "/var/www/html/app/console oro:install --no-interaction --symlink --drop-database --env=prod --user-name=admin --user-firstname=Alan --user-lastname=Sun --user-password=admin1111 --user-email=rick1125@gmail.com --organization-name='FM China' --sample-data=n --application-url=https://www.fmkol.com" www-data
update:
	runuser -c '/var/www/html/app/console oro:platform:update --force --env=prod' www-data
clean:
	chown www-data.www-data app/cache -R
	runuser -c '/var/www/html/app/console cache:clear --env=prod' www-data
message:
	runuser -c '/var/www/html/app/console translation:update en --force --env=prod' www-data
dump:
	runuser -c '/var/www/html/app/console doctrine:schema:update --dump-sql' www-data
load:
	runuser -c '/var/www/html/app/console oro:migration:load --force' www-data
