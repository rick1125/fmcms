<?php
/**
 * Created by PhpStorm.
 * User: rick
 * Date: 2017/11/27
 * Time: 15:28
 */
namespace FM\PlatformBundle\ImportExport\Processor;

use FM\PlatformBundle\Entity\Resource;
use Oro\Bundle\ImportExportBundle\Processor\ImportProcessor;

class FillBuProcessor extends ImportProcessor
{
    public function process($item)
    {
        /** @var Resource $entity */
        $entity = parent::process($item);

        if ($entity) {
            $buId = $this->context->getOption('owner');
            $businessUnit = $this->strategy->getBusinessUnit($buId);
            $platform = $this->strategy->getPlatform($entity->getLink());
            if ($platform) {
                $entity->setPlatform($platform);
            }
            if (empty(trim($entity->getPriceA()))) {
                $entity->setPriceA(0);
            }
            if (empty(trim($entity->getPriceB()))) {
                $entity->setPriceB(0);
            }
            $this->strategy->upsertChannel($entity);
            $entity->setOwner($businessUnit);
            $entity->setLinkHash(md5($entity->getLink()));
//            $entity->setCreatedAt(new \DateTime('now'));
//            $entity->setCreatedBy($user);
        }

        return $entity;
    }
}
