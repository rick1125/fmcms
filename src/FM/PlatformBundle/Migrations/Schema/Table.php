<?php
/**
 * Created by PhpStorm.
 * User: rick
 * Date: 2018/1/21
 * Time: 22:25
 */

namespace FM\PlatformBundle\Migrations\Schema;

final class Table
{
    const CONTRACT = 'fm_contract';
    const PROJECT = 'fm_project';
    const RESOURCE = 'fm_resource';
    const RESULT = 'fm_resource_result';
    const CHANNEL = 'fm_channel';
    const PLATFORM = 'fm_platform';
}
