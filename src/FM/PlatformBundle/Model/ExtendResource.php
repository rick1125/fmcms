<?php

namespace FM\PlatformBundle\Model;

use FM\PlatformBundle\Entity\AbstractEntity;

class ExtendResource extends AbstractEntity
{
    /**
     * Constructor
     *
     * The real implementation of this method is auto generated.
     *
     * IMPORTANT: If the derived class has own constructor it must call parent constructor.
     */
    public function __construct()
    {
    }
}
