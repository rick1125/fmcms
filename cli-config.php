<?php
/**
 * for doctrine cli
 */
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// replace with file to your own project bootstrap
require_once 'vendor/autoload.php';

// replace with mechanism to retrieve EntityManager in your app
$paths = ['src/Rhizen/PlatformBundle/Entity'];

$params = [
  'driver' => 'pdo_mysql',
  'dbname' => 'rhizen',
  'user' => 'root',
  'password' => 'admin',
  'host' => 'mysql',
  'port' => 3306
];

$config = Setup::createAnnotationMetadataConfiguration($paths, false);
$entityManager = EntityManager::create($params, $config);

return ConsoleRunner::createHelperSet($entityManager);
